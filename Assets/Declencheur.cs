using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Declencheur : MonoBehaviour
{
    int cpt = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        cpt++;
        Debug.Log("Entrée : " + cpt + " boites");
    }

    private void OnTriggerExit(Collider other)
    {
        cpt--;
        Debug.Log("Sortie : " + cpt + " boites");
    }
}
