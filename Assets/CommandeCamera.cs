using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandeCamera : MonoBehaviour
{
    public Camera Main;
    public Camera interieur;
    public Camera moufle;
    
    // Start is called before the first frame update
    void Start()
    {
        Main.enabled = true;
        interieur.enabled = false;
        moufle.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            Main.enabled = true;
            interieur.enabled = false;
            moufle.enabled = false;
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            Main.enabled = false;
            interieur.enabled = true;
            moufle.enabled = false;
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            Main.enabled = false;
            interieur.enabled = false;
            moufle.enabled = true;
        }

    }
}
